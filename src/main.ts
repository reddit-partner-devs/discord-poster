import { Devvit, getSetting } from '@devvit/public-api';
import { PostSubmit, Metadata } from '@devvit/protos';
Devvit.use(Devvit.Types.HTTP);

Devvit.addSettings([
    {
        type: 'string',
        name: 'self_post_url',
        label: 'Webhook URL',
    },
    {
        type: 'string',
        name: 'avatar_url',
        label: 'URL to the avatar image to use',
    },
    {
        type: 'number',
        name: 'self_post_color',
        label: 'The color to use for self posts as a base 10 integer',
    },
    {
        type: 'group',
        label: 'Link Posts',
        fields: [
            {
                type: 'string',
                name: 'link_post_url',
                label: 'Separate URL to use for link posts (if enabled)',
            },
            {
                type: 'boolean',
                name: 'separate_urls',
                label: 'Use a separate URL for link posts?',
            },
            {
                type: 'number',
                name: 'link_post_color',
                label: 'The color to use for link posts as a base 10 integer',
            },
        ],
    },
]);

async function generateEmbed(request: PostSubmit, metadata: Metadata) {
    return {
        'username': `r/${request.subreddit!.name}`,
        'avatar_url': await getSetting('avatar_url', metadata),
        'embeds': [
            {
                'url': request.post!.url,
                'type': 'rich',
                'title': request.post!.title,
                'description': request.post!.isSelf ? `**${request.post!.title}**\n${request.post!.selftext}` : request.post!.url,
                'color': request.post!.isSelf ? await getSetting('self_post_color', metadata) : await getSetting('link_post_color', metadata),
                'author': {
                    'url': `https://reddit.com/user/${request.author!.name}`,
                    'name': request.author!.name,
                }
            }
        ]
    }
};

async function getWebhookURL(request: PostSubmit, metadata: Metadata): Promise<string> {
    if (await getSetting('separate_urls', metadata)) {
        if (request.post!.isSelf) {
            return (await getSetting('self_post_url', metadata))!
        }
        return (await getSetting('link_post_url', metadata))!
    }
    return (await getSetting('self_post_url', metadata))!
};

Devvit.addTrigger({
    event: Devvit.Trigger.PostSubmit,
    async handler(request: PostSubmit, metadata?: Metadata) {
        console.log(JSON.stringify(await generateEmbed(request, metadata!)))
        const response = await fetch(await getWebhookURL(request, metadata!), {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(await generateEmbed(request, metadata!)),
        })
        return {
            success: true,
            message: `Invoked HTTP request on post: ${request.post!.id}. Completed with status: ${response.status}`,
        };
    },
});

export default Devvit;
